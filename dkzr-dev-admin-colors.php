<?php
/**
 * Plugin Name: Development Admin Colors
 * Plugin URI: https://gitlab.com/dkzr-wordpress/plugins/dkzr-dev-admin-colors
 * Update URI: https://api.dkzr.nl/wp/update-check/
 * Composer Package Name: dkzr/dev-admin-colors
 * Description: Display notice and custom color scheme for admin area in development environment
 * Version: 2.2.1
 * Author: Joost de Keijzer <j@dkzr.nl>
 * Text Domain: dkzr-dev-admin-colors
 * Domain Path: languages
 * License: GPL2
 */

class dkzrDevAdminColors {
  public function __construct() {
    add_action( 'admin_init', [$this, 'register_admin_color_schemes'], 2 );
    add_filter( 'get_user_metadata', [$this, 'get_user_metadata'], 1, 4 );

    add_action( 'wp_head', [$this, 'wp_head'], 9999 );

    add_action('admin_notices', [$this,'display_notices']);
    add_action('network_admin_notices', [$this,'display_notices']);
  }

  public function register_admin_color_schemes() {
    global $_wp_admin_css_colors;
    $_wp_admin_css_colors = array();

    $suffix  = is_rtl() ? '-rtl' : '';
    $suffix .= SCRIPT_DEBUG ? '' : '.min';

    wp_admin_css_color(
      'dkzr-dev',
      _x( 'Development', 'admin color scheme', 'dkzr-dev-admin-colors' ),
      admin_url( "css/colors/sunrise/colors$suffix.css" ),
      array( '#b43c38', '#cf4944', '#dd823b', '#ccaf0b' ),
      array(
        'base'    => '#f3f1f1',
        'focus'   => '#fff',
        'current' => '#fff',
      )
    );
  }

  public function get_user_metadata( $value, $object_id, $meta_key, $single ) {
    if ( 'admin_color' == $meta_key ) {
      return [ 'dkzr-dev' ];
    }

    return $value;
  }

  public function wp_head() {
    if ( is_user_logged_in() ) {
      $suffix  = is_rtl() ? '-rtl' : '';
      wp_enqueue_style( 'dkzr-dev-admin-colors-css', plugin_dir_url( __FILE__ ) . "css/sunrise-adminbar$suffix.css" );
    }
  }

  public function display_notices() {
    if( ! function_exists('get_plugin_data') ){
      require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    }
    $plugin = get_plugin_data( __FILE__ );

    $mu_plugins = get_mu_plugins();

    $notice = sprintf( __('You\'re currently viewing the development environment <strong>%s</strong>, not the live site.', 'dkzr-dev-admin-colors' ), wp_parse_url( get_home_url(), PHP_URL_HOST ) );
    if ( empty( $mu_plugins[ basename( __FILE__ ) ] ) ) {
      $notice .= '<br>' . sprintf( __('Disable the <a href="%s">%s</a> plugin to remove this notice and restore the regular admin colors.', 'dkzr-dev-admin-colors'), admin_url( 'plugins.php' ), $plugin['Name'] );
    }

    printf( '<div class="notice notice-info"><p>%s</p></div>', $notice );
  }

  public function install_mu_plugin() {
    if( ! function_exists('get_plugin_data') ){
      require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    }
    $plugin = get_plugin_data( __FILE__ );

    $lines = [
      '<?php',
      '/**',
      sprintf(' * Plugin Name: MU %s', $plugin['Name'] ),
      sprintf(' * Description: Activates <em>%s</em> when available in the normal plugins directory.', $plugin['Name'] ),
    ];

    foreach( [ 'PluginURI', 'Version', 'Author', 'AuthorURI' ] as $key ) {
      if ( !empty( $plugin[$key] ) ) {
        $lines[] = sprintf( ' * %s: %s', preg_replace( '#([a-z])([A-Z])#', '$1 $2', $key ), $plugin[$key] );
      }
    }

    $lines = array_merge( $lines, [
      ' */',
      sprintf( '@include_once( WP_PLUGIN_DIR . \'/%s\' );', preg_replace( '#^' . WP_PLUGIN_DIR . '/#', '', __FILE__ ) ),
    ] );

    file_put_contents( WPMU_PLUGIN_DIR . '/' . basename( __FILE__ ), implode( "\n", $lines ) );

    return true;
  }

  public function remove_mu_plugin() {
    @unlink( WPMU_PLUGIN_DIR . '/' . basename( __FILE__ ) );

    return true;
  }
}
$dkzr_dev_admin_colors = new dkzrDevAdminColors();
